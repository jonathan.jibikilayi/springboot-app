package employees.controllers;


import employees.Repository.EmployeesRepo;
import employees.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = {"*"})
@RestController
public class TestController {
@Autowired
private EmployeesRepo employeesRepo;
	

	@RequestMapping(value = "/employees", method = RequestMethod.GET, produces = "application/json")
	public List<Employee> firstPage() {
		return employeesRepo.findAll();
	}


	@PutMapping("/update")
	public List<Employee> updateemployer( @RequestBody Employee employee)
	{
		employeesRepo.save(employee);
		return  employeesRepo.findAll();

	}
	@PostMapping("/add")
	public List<Employee> addemployer( @RequestBody Employee employee)
	{
		employeesRepo.save(employee);
		return  employeesRepo.findAll();
	}
	@DeleteMapping("/delete/{empid}")
	public  void deleteemployer(@PathVariable("empid") final String empid)
	{
		employeesRepo.deleteById(empid);
	}

	@RequestMapping(value = "/version", method = RequestMethod.GET, produces = "application/json")
	public Map<String, Object> version() {
		String version = System.getenv("BUILD_VERSION");
		Map<String, Object> v = new HashMap<>();
		v.put("version", version != null? version: "unknown");
		return v;

	}

}
